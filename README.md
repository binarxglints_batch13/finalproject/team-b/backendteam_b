# RemindMe API

This repository contain source code for RemindMe server side, a final project from Glints x Binar Academy Batch 13

## Documentation

All endpoint are listed in Postman Documenter which you can see [here](https://documenter.getpostman.com/view/14563768/TzzGFt9w).

## Run Locally

If you wish to run this project locally follow below step:

Clone the project

```bash
  git clone https://gitlab.com/binarxglints_batch13/finalproject/team-b/backendteam_b.git
```

Go to the project directory

```bash
  cd backendteam_b
```

Install dependencies

```bash
  npm install
```

Start the server in Windows environtment with **dev** and linux with **dev1**

```bash
  npm run dev
```

```bash
  npm run dev1
```

## Environment Variables

To run this project, you will need to add the required environment variables to your .env file which is list inside [.env.example](https://gitlab.com/binarxglints_batch13/finalproject/team-b/backendteam_b/-/blob/development/.env.example) file.

## Acknowledgements

- [Remindme Web App](https://remindme-web.herokuapp.com/)
- [RemindMe Android App](https://drive.google.com/file/d/1mNutQk4ai5bL27iZM4HxN8_9b5tvD81i/view?usp=sharing)

## Authors

- [Gilang Sukma Wijaya](https://gitlab.com/Gilangsukma.w)
- [Faizul Fuadi](https://gitlab.com/ffuad13)
- [Pandji Bayu Satria](https://gitlab.com/PandjiBayu)

![Logo](https://res.cloudinary.com/remindme/image/upload/v1632235972/notes_dev/jdq6dka1m1wpbz9vogio.png)
